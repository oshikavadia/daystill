
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Oshi
 */
public class DaysTill {

    public static void main(String[] args) {
        Date currentDate = null, argDate = null;
//        ^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yy");
        String currentDateString = dateFormat.format(new Date());
        if (args.length != 1) {
            printError();
            System.exit(1);
        }
        boolean isValidDate = args[0].matches("^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$");
        if (args[0].equalsIgnoreCase("-h") || !isValidDate) {
            printHelp();
            System.exit(0);
        }

        args[0] = args[0].replace('/', ' ');

        try {
            currentDate = dateFormat.parse(currentDateString);
            argDate = dateFormat.parse(args[0]);
        } catch (ParseException ex) {
//            printError();
            ex.printStackTrace();
            System.exit(1);
        }

        long diff = argDate.getTime() - currentDate.getTime();
        System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
    }

    public static void printHelp() {
        System.out.println("DaysTill D "
                + "\nD = dd/mm/yy\n"
                + "-h = This Prompt");
    }

    public static void printError() {
        System.out.println("This program takes in one argument. Type -h for more information...");
    }
}
